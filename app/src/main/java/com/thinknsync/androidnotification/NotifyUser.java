package com.thinknsync.androidnotification;

import com.thinknsync.objectwrappers.PendingIntentWrapper;

/**
 * Created by shuaib on 6/17/17.
 */

public interface NotifyUser<T> {
    void init(NotificationObject notificationObject, PendingIntentWrapper action);
    void showNotification();
    T getNotificationObject();
}
