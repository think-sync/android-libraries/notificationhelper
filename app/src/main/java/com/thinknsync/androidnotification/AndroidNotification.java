package com.thinknsync.androidnotification;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;

import androidx.core.app.NotificationCompat;

import com.thinknsync.objectwrappers.AndroidContextWrapper;
import com.thinknsync.objectwrappers.PendingIntentWrapper;

import static android.content.Context.NOTIFICATION_SERVICE;

/**
 * Created by shuaib on 6/17/17.
 */

public class AndroidNotification implements NotifyUser<Notification> {

    private Context context;
    private Notification notification;

    public AndroidNotification(AndroidContextWrapper contextWrapper){
        this.context = contextWrapper.getFrameworkObject();
    }

    @Override
    public void init(NotificationObject notificationObject, PendingIntentWrapper action) {
        String channel = getNotificationChannel();
        notification  = new NotificationCompat.Builder(context, channel)
                .setContentTitle(notificationObject.getTitle())
                .setContentText(notificationObject.getContent())
                .setSmallIcon(notificationObject.getImage())
                .setAutoCancel(notificationObject.isAutocancel())
                .setOngoing(notificationObject.isOngoing())
                .setPriority(Notification.PRIORITY_HIGH)
                .setLights(Color.RED, 3000, 3000)
                .build();

        if(action != null){
            notification.contentIntent = action.getFrameworkObject();
        }
        notification.defaults |= Notification.DEFAULT_VIBRATE;
        notification.defaults |= Notification.DEFAULT_SOUND;
    }

    private String getNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
            return  createChannel();
        else {
            return "";
        }
    }

    @TargetApi(26)
    private String createChannel() {
        NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        String id = "notificationId";
        String name = "notificationName";
        int importance = NotificationManager.IMPORTANCE_LOW;

        NotificationChannel mChannel = new NotificationChannel(id, name, importance);

        mChannel.enableLights(true);
        mChannel.setLightColor(Color.RED);
        if (mNotificationManager != null) {
            mNotificationManager.createNotificationChannel(mChannel);
        }
        return id;
    }

    @Override
    public void showNotification() {
        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);

        notificationManager.notify(1000, notification);
    }

    @Override
    public Notification getNotificationObject() {
        return notification;
    }
}
