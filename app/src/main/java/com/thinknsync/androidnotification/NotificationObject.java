package com.thinknsync.androidnotification;

import com.thinknsync.convertible.BaseConvertible;

import org.json.JSONException;
import org.json.JSONObject;

public final class NotificationObject extends BaseConvertible<NotificationObject> {

    public static final String tag = "notificationObject";
    public static final String key_title = "title";
    public static final String key_content = "content";
    public static final String key_image = "image";
    public static final String key_auto_cancel = "auto_cancel";
    public static final String key_ongoing = "ongoing";

    private String title;
    private String content;
    private int image;
    private boolean autocancel = true;
    private boolean ongoing = false;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public boolean isAutocancel() {
        return autocancel;
    }

    public void setAutocancel(boolean autocancel) {
        this.autocancel = autocancel;
    }

    public boolean isOngoing() {
        return ongoing;
    }

    public void setOngoing(boolean ongoing) {
        this.ongoing = ongoing;
    }

    @Override
    public NotificationObject fromJson(JSONObject jsonObject) throws JSONException {
        setTitle(jsonObject.getString(key_title));
        setContent(jsonObject.getString(key_content));
        setImage(jsonObject.getInt(key_image));
        setAutocancel(jsonObject.getBoolean(key_auto_cancel));
        setOngoing(jsonObject.getBoolean(key_ongoing));
        return this;
    }

    @Override
    public JSONObject toJson() throws JSONException {
        return new JSONObject().put(key_title, getTitle())
                .put(key_content, getContent())
                .put(key_image, getImage())
                .put(key_auto_cancel, isAutocancel())
                .put(key_ongoing, isOngoing());
    }
}
